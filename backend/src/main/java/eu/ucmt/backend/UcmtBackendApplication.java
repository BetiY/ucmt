package eu.ucmt.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UcmtBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(UcmtBackendApplication.class, args);
    }

}
