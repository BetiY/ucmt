CREATE TABLE "users" (
	"id" serial NOT NULL,
	"first_name" varchar(128) NOT NULL,
	"last_name" varchar(128) NOT NULL,
	"email" varchar(255) NOT NULL UNIQUE,
	"email2" varchar(255),
	"password" varchar(255) NOT NULL,
	"parent_name" varchar(255),
	"mobile" varchar(13),
	"mobile2" varchar(13),
	CONSTRAINT "users_pk" PRIMARY KEY ("id")
);



CREATE TABLE "props" (
	"id" serial NOT NULL,
	"name" varchar(128) NOT NULL,
	"description" TEXT NOT NULL,
	"count" integer NOT NULL,
	"club_id" integer NOT NULL,
	CONSTRAINT "props_pk" PRIMARY KEY ("id")
);



CREATE TABLE "clubs" (
	"id" serial NOT NULL,
	"name" varchar(128) NOT NULL,
	"description" TEXT,
	"location" TEXT,
	"creator" integer NOT NULL,
	CONSTRAINT "clubs_pk" PRIMARY KEY ("id")
);



CREATE TABLE "groups" (
	"id" serial NOT NULL,
	"name" varchar(128) NOT NULL,
	"desciption" TEXT NOT NULL,
	"club_id" integer NOT NULL,
	CONSTRAINT "groups_pk" PRIMARY KEY ("id")
);

CREATE TYPE "user_to_group_type" AS ENUM ('member', 'teacher');

CREATE TABLE "user_to_group" (
	"group_id" integer NOT NULL,
	"user_id" integer NOT NULL,
	"type" user_to_group_type NOT NULL,
	CONSTRAINT "user_to_group_pk" PRIMARY KEY ("group_id","user_id")
) ;



CREATE TABLE "classes" (
	"id" serial NOT NULL,
	"day" varchar(10) NOT NULL,
	"start" TIME NOT NULL,
	"end" TIME NOT NULL,
	"group" integer NOT NULL,
	"start_date" DATE NOT NULL,
	"end_date" DATE NOT NULL,
	CONSTRAINT "classes_pk" PRIMARY KEY ("id")
) ;



CREATE TABLE "user_to_class" (
	"class_id" integer NOT NULL,
	"user_id" integer NOT NULL,
	"date_participated" DATE NOT NULL,
	CONSTRAINT "user_to_class_pk" PRIMARY KEY ("class_id","user_id","date_participated")
) ;



CREATE TABLE "booking" (
	"id" serial NOT NULL,
	"prop_id" integer NOT NULL,
	"user_id" integer NOT NULL,
	"reason_id" integer NOT NULL,
	"count" integer NOT NULL,
	"given_on" TIMESTAMP WITH TIME ZONE NOT NULL,
	"given_by" integer NOT NULL,
	"returned_on" TIMESTAMP WITH TIME ZONE,
	"returned_to" integer,
	CONSTRAINT "booking_pk" PRIMARY KEY ("id","prop_id","user_id","reason_id")
) ;



CREATE TABLE "reason" (
	"id" serial NOT NULL,
	"name" varchar(128) NOT NULL,
	"description" TEXT,
	"from" DATE NOT NULL,
	"to" DATE NOT NULL,
	"club_id" integer NOT NULL,
	CONSTRAINT "reason_pk" PRIMARY KEY ("id")
) ;




ALTER TABLE "props" ADD CONSTRAINT "props_fk0" FOREIGN KEY ("club_id") REFERENCES "clubs"("id");

ALTER TABLE "clubs" ADD CONSTRAINT "clubs_fk0" FOREIGN KEY ("creator") REFERENCES "users"("id");

ALTER TABLE "groups" ADD CONSTRAINT "groups_fk0" FOREIGN KEY ("club_id") REFERENCES "clubs"("id");

ALTER TABLE "user_to_group" ADD CONSTRAINT "user_to_group_fk0" FOREIGN KEY ("group_id") REFERENCES "groups"("id");
ALTER TABLE "user_to_group" ADD CONSTRAINT "user_to_group_fk1" FOREIGN KEY ("user_id") REFERENCES "users"("id");

ALTER TABLE "classes" ADD CONSTRAINT "classes_fk0" FOREIGN KEY ("group") REFERENCES "groups"("id");

ALTER TABLE "user_to_class" ADD CONSTRAINT "user_to_class_fk0" FOREIGN KEY ("class_id") REFERENCES "classes"("id");
ALTER TABLE "user_to_class" ADD CONSTRAINT "user_to_class_fk1" FOREIGN KEY ("user_id") REFERENCES "users"("id");

ALTER TABLE "booking" ADD CONSTRAINT "booking_fk0" FOREIGN KEY ("prop_id") REFERENCES "props"("id");
ALTER TABLE "booking" ADD CONSTRAINT "booking_fk1" FOREIGN KEY ("user_id") REFERENCES "users"("id");
ALTER TABLE "booking" ADD CONSTRAINT "booking_fk2" FOREIGN KEY ("reason_id") REFERENCES "reason"("id");
ALTER TABLE "booking" ADD CONSTRAINT "booking_fk3" FOREIGN KEY ("given_by") REFERENCES "users"("id");
ALTER TABLE "booking" ADD CONSTRAINT "booking_fk4" FOREIGN KEY ("returned_to") REFERENCES "users"("id");

ALTER TABLE "reason" ADD CONSTRAINT "reason_fk0" FOREIGN KEY ("club_id") REFERENCES "clubs"("id");
